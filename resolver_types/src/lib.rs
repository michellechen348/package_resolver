use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize)]
pub struct PackageRequest {
    packages: Vec<RequestedPackage>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RequestedPackage {
    name: String,
    version: String,
    // depth: Option<u32>,
}

/// Struct expected to be used by the resolver libraries to represent a resolved package
#[derive(Debug, Serialize, Deserialize)]
pub struct ResolvedPackage {
    name: String,
    resolved_version: String,
    download_link: String,
}

pub trait Resolver {
    fn resolve(requested_packages_list: Vec<RequestedPackage>) -> HashMap<String, ResolvedPackage>;
}
