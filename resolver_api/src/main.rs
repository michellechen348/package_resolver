use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::routing::post;
use axum::{routing::get, Json, Router};
use clap::Parser;
use log::{debug, error};
use resolver_types::PackageRequest;
use std::net::SocketAddr;
use std::str::FromStr;

/// A web API to resolve NPM dependencies
#[derive(Parser, Debug)]
#[command(author, version, about)]
struct Args {
    /// Address to bind the API to
    #[arg(short, long, default_value = "0.0.0.0:8080")]
    binding_address: String,
}

fn error_and_panic(error_message: String) {
    error!("{}", error_message);
    panic!("{}", error_message);
}

#[tokio::main]
async fn main() {
    match env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("warn"))
        .try_init()
    {
        Ok(_) => debug!("Logger initialized!"),
        Err(e) => error!("Failed to initialize logger: {e}"),
    };

    let args = Args::parse();

    let app = Router::new()
        .route("/", get(root))
        .route("/package_request", post(package_request));

    let address = SocketAddr::from_str(&args.binding_address).unwrap_or_else(|e| {
        panic!("Failed to parse address {}: {e}", &args.binding_address);
    });

    axum::Server::bind(&address)
        .serve(app.into_make_service())
        .await
        .unwrap_or_else(|e| {
            error_and_panic(format!("Failed to start serving: {e}"));
        });
}

/// Simply greets anyone coming to the root
async fn root() -> &'static str {
    "Hello World!"
}

/// Resolves the requested package and its dependencies.
/// Returns a list of packages, their versions, and their download link
async fn package_request(Json(payload): Json<PackageRequest>) -> impl IntoResponse {
    debug!("Received request: {:#?}", payload);

    (StatusCode::OK, "Success?")
}
